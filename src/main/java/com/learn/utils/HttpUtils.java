package com.learn.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HttpUtils {

	/**
	 * 拷贝网上图片到本地文件
	 */
	public static void webResourcesToLocal(String urltxt, File file) throws Exception {
		FileUtils.copyInputStreamToFile(getInputStreamFromUrl(urltxt), file);
	}

	/**
	 * 获取jsoup的根节点
	 */
	public static Document getJsoupDocument(String url) throws Exception {
		long start = System.currentTimeMillis();
		Document doc = Jsoup.connect(url)
				// .header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64;
				// rv:33.0) Gecko/20100101 Firefox/33.0")
				.userAgent("Mozilla").timeout(10000).cookie("auth", "token").get();
		long end = System.currentTimeMillis();
		System.out.println("getJsoupDocument time=" + (end - start));
		return doc;
	}

	public static String getStringFromUrl(String urltxt, String encode) throws Exception {
		return IOUtils.toString(getInputStreamFromUrl(urltxt), encode);
	}

	public static InputStream getInputStreamFromUrl(String urltxt) throws IOException {
		URL url = new URL(urltxt);
		HttpURLConnection urlcon = (HttpURLConnection) url.openConnection();
		urlcon.setRequestProperty("User-Agent", "Mozilla");
		urlcon.setConnectTimeout(30000);
		urlcon.setReadTimeout(30000);
		InputStream is2 = urlcon.getInputStream();
		return is2;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
