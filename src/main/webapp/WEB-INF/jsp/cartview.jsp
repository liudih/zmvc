<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="0"/>
	<title>Shopping Cart - TOMTOP</title>
	<link rel="icon" href="" type="image/x-icon">
	<meta name="description" content="No Regular No Business. TOMTOP offers hot gadgets at the best price online. Enjoy fast shipping & excellent service." />
	<meta name="keywords" content="TOMTOP, hot gadgets, cheap rc models" />
	
	<script src="http://cdn.bootcss.com/less.js/1.7.0/less.min.js"></script>
	<link rel="stylesheet" href="shopping.less" /> 
	
	<link rel="stylesheet" href="@controllers.base.routes.Assets.at("css/tomtop.css")">
	<link rel="stylesheet" href="@controllers.base.routes.Assets.at("css/base/country.css")" />
	<link rel="stylesheet" type="text/css" href="@controllers.base.routes.Assets.at("css/shopping.css")" />
	<link rel="stylesheet" type="text/css" href="@controllers.base.routes.Assets.at("css/newshopping.css")" />
	<script src="@controllers.base.routes.Assets.at("lib/jquery/jquery.min.js")"></script>
	<script src="@controllers.base.routes.Assets.at("js/lib/public.js")?@version.BuildVersion.build()"></script>
	<script src="@controllers.base.routes.Assets.at("js/lib/main.js")?@version.BuildVersion.build()"></script>
	</head>
	<body>
		<div class="container@if(isManual){ printPage}" id="top_s">
			<div class="shoppingCart_top" >
				<div class="topNavigation_logo navigationCart_logo lineBlock">
			    		<div class="topNav_logoT"><a href="/"><img src="@controllers.base.routes.Assets.at("img/Tomtop_logo0603_01.png")" /></a></div>
			            <div class="topNav_logoB"><img src="@controllers.base.routes.Assets.at("img/Tomtop_logo0603_02.png")" /></div>
			    </div>
				
					 
					<ul class="shop_process">
				    </ul>
			</div>
		</div>
		<div class="hotProduct_box boxFramework">
	    <div class="clear"></div>
		</div>
		<!--购物车底部-->
		<div class="shopping_bottom">
		    <div>
		        <a class="icon_paypal"></a>
		        <a class="icon_visa"></a>
		        <a class="icon_qiwi"></a>
		        <a class="icon_ehb"></a>
		        <a class="icon_boleto"></a>
		        <a class="icon_wiae"></a>
		        <a class="icon_norton" href="javascript:vrsn_splash()"></a>
		        <a class="icon_money"></a>
		    </div>
		    <div>
		        <a class="icon_china"></a>
		        <a class="icon_ems"></a>
		        <a class="icon_dhl"></a>
		        <a class="icon_ups"></a>
		    </div>
		    <p>Copyright © 2004- 2015 TOMTOP Inc. All Rights Reserved. (Build: @version.BuildVersion.build())</p>
		</div>
	</body>
</html>