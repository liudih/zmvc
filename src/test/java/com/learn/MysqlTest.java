package com.learn;

import java.sql.*;
import java.util.Date;

import org.junit.Test;

import com.learn.utils.DateUtils;

public class MysqlTest {

	@Test
	public void test() throws Exception {
		Connection conn = getConnection();
		Statement sql_statement = conn.createStatement();
		String nowdate = DateUtils.formatDate(new Date());
		String sql1 = "insert into article(title, body, createdate) values('t', 'test', '" + nowdate + "')";
		int flag = sql_statement.executeUpdate(sql1);
		System.out.println(flag);
		
		String query = "select * from article";  
        ResultSet result = sql_statement.executeQuery(query);  
        while (result.next()) {  
            int number = result.getInt("id");  
            String name = result.getString("title");  
            String mathScore = result.getString("createdate");  
            System.out.println(" " + number + " " + name + " " + mathScore);  
        }  
        
		if (!conn.isClosed()) {
			System.out.println("数据库连接成功!");
		}

		sql_statement.close();
		conn.close();
	}

	public static Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/novel";
		String username = "root";
		String password = "person";
		Connection con = DriverManager.getConnection(url, username, password);
		return con;
	}

}
