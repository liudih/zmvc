package com.learn;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import com.learn.utils.DateUtils;
import com.learn.utils.HttpUtils;

public class NovelTest {

	@Test
	public void test() throws Exception {
		Document doc = HttpUtils.getJsoupDocument("http://m.biquge.cm/wapbook/837.html");
		Elements ele = doc.select(".chapter9 a");
		/*
		 * Collection.sort(list);升序排列 Collections.sort(list,
		 * Collections.reverseOrder());降序排列 Collections.reverse(list);反转排序
		 */
		Connection conn = getConnection();
		String nowdate = DateUtils.formatDate(new Date());
		for (int i = ele.size()-5; i >= 0; i--) {
			Element e = ele.get(i);
			String title = e.html();
			title = StringEscapeUtils.escapeHtml4(GbkToUtf8(title));
			Statement sql_statement = conn.createStatement();
			String query = "select count(*) c from article where title='"+title+"'";
			ResultSet result = sql_statement.executeQuery(query);
			result.last();
			if(result.getInt("c")==0){
				String url = e.attr("href");
				System.out.println(title+"=="+url);
				Document doc2 = HttpUtils.getJsoupDocument("http://m.biquge.cm/"+url);
				String body = doc2.select("#nr1").first().html();
				body = StringEscapeUtils.escapeHtml4(GbkToUtf8(body));
				String sql1 = "insert into article(title, body, createdate) values('"+title+"', '"+body+"', '" + nowdate + "')";
				int flag = sql_statement.executeUpdate(sql1);
				System.out.println("executeUpdate==="+flag);
			}
			sql_statement.close();
		}
		if (!conn.isClosed()) {
			System.out.println("数据库连接成功!");
		}
		conn.close();
	}
	
	private static Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/novel";
		String username = "root";
		String password = "person";
		Connection con = DriverManager.getConnection(url, username, password);
		return con;
	}
	
	private String GbkToUtf8(String html){
		String a = "";
		try {
			File file = new File("target/noveltemp");
			FileUtils.writeStringToFile(file, html, Charset.forName("utf-8"));
			a = FileUtils.readFileToString(file, "utf-8");
		} catch (Exception e) {
		}
		return a;
	}

}
