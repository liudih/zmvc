package com.learn.http;

import static org.junit.Assert.*;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Ignore;
import org.junit.Test;

import com.learn.utils.HttpUtils;

public class JsoupTest {
	
	//@Ignore("test")
	@Test
	public void test() throws Exception {
		// 天天排行 http://www.ttpaihang.com/vote/rankdetail-94287.html
		// String[] javArray = {"https://www.javbus2.com/NFDM-345"};
		Document doc = HttpUtils.getJsoupDocument("https://www.javbus2.com/PARM-068");
		String title = doc.title();
		System.out.println("title=" + title);
		System.out.println("img.size()==" + doc.select(".photo-frame img").size());
		Element img = doc.select(".photo-frame img").first();
		String imgurl = img.attr("src");
		System.out.println("img==" + img.attr("src"));

		HttpUtils.webResourcesToLocal(imgurl, new File("target/aa.jpg"));
	}

	//@Ignore("test")
	@Test
	public void test2() throws Exception {
		String html = HttpUtils.getStringFromUrl("http://m.biquge.cm/wapbook/837.html", "gbk");
		//可以实现转utf-8
//		FileUtils.writeStringToFile(new File("target/html.html"), html, Charset.forName("utf-8"));
//		String a = FileUtils.readFileToString(new File("target/html.html"), "utf-8");
//		System.out.println(a);
		
//		StringWriter os = new StringWriter();
//		IOUtils.write(html.getBytes(), os, Charset.forName("utf-8"));
//		System.out.println(os.toString());
		//System.out.println(new String(html.getBytes(), "utf-8"));
	}

}
