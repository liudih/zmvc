package com.learn.freemark;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

public class FreemarkTest {

	@Test
	public void test() throws Exception {
		// freemarker
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_0);
		System.out.println(getClass().getResource("/template").toURI().toString());
		String pathDir = getClass().getResource("/template").getPath();
		System.out.println(pathDir);
		cfg.setDirectoryForTemplateLoading(new File(pathDir));
		

		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);

		Map<String, Object> root = new HashMap<>();
		root.put("user", "Dicky");
		Map<String, Object> mjson = new HashMap<>();
		mjson.put("code", "aaa");
		mjson.put("name", "bbbbb");
		root.put("latestProduct", mjson);

		/* Get the template (uses cache internally) */
		Template temp = cfg.getTemplate("test.ftl");

		/* Merge data-model with template */
		//Writer out = new OutputStreamWriter(System.out);
		Writer out = new StringWriter();
		OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream("target/t2.html"));
		temp.process(root, fw);
		temp.process(root, out);
		fw.close();
		System.out.println(out.toString());
	}

}
